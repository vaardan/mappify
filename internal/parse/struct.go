package parse

import (
	"fmt"
	"go/types"

	"golang.org/x/tools/go/packages"
)

// Struct parses provided packages and tries to find a structure with structureName.
// Path can be an absolute, relative or canonical (i.e. as imported in go) path.
func Struct(structureName, path string) (types.Object, error) {
	cfg := &packages.Config{
		Mode: packages.NeedTypes,
	}

	pkgs, err := packages.Load(cfg, path)
	if err != nil {
		return nil, err
	}

	// one path should produce only one package, all _test.go files are excluded by default
	if len(pkgs) > 1 {
		return nil, fmt.Errorf("directory '%s' contains multiple package declarations", path)
	}
	pkg := pkgs[0]

	if len(pkg.Errors) != 0 {
		return nil, pkg.Errors[0]
	}

	if len(pkg.TypeErrors) != 0 {
		return nil, pkg.TypeErrors[0]
	}

	if pkg.IllTyped {
		// pkg.ID is the canonical path
		return nil, fmt.Errorf("package '%s' is ill typed", pkg.ID)
	}

	if pkg.Types == nil {
		return nil, fmt.Errorf("can't load package '%s'", pkg.ID)
	}

	obj := pkg.Types.Scope().Lookup(structureName)
	if obj == nil {
		return nil, fmt.Errorf("package '%s' doesn't cointain strcut '%s'", pkg.ID, structureName)
	}

	named, ok := obj.Type().(*types.Named)
	if !ok {
		return nil, fmt.Errorf("'%s' isn't a struct", structureName)
	}

	_, ok = named.Underlying().(*types.Struct)
	if !ok {
		return nil, fmt.Errorf("'%s' isn't a struct", structureName)
	}

	return obj, nil
}

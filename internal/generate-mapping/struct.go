package generatemapping

import (
	"fmt"
	"go/types"
	"strings"

	j "github.com/dave/jennifer/jen"
)

const tagKey = "mappify"

type StructOpts struct {
	FunctionName   string
	InArgumentName string
	InTypePointer  bool
	OutTypePointer bool
	ErrorInReturn  bool
}

func Struct(in, out types.Object, opts StructOpts) (string, error) {
	err := validateStructs(in, out)
	if err != nil {
		return "", err
	}

	file := j.NewFilePath("mappify")

	file.Func().
		Id(opts.FunctionName).
		ParamsFunc(func(inputParamsGroup *j.Group) {
			qualifier := makeQualifier(in.Type())
			if opts.InTypePointer {
				qualifier = j.Op("*").Add(qualifier)
			}

			inputParamsGroup.Id(opts.InArgumentName).Add(qualifier)
		}).
		Do(func(statement *j.Statement) {
			qualifier := makeQualifier(out.Type())
			if opts.OutTypePointer {
				qualifier = j.Op("*").Add(qualifier)
			}
			if opts.ErrorInReturn {
				statement.Params(qualifier, j.Error())
				return
			}
			statement.Add(qualifier)
		}).
		BlockFunc(makeFuncBody(in, out, opts))

	return fmt.Sprintf("%#v", file), nil
}

type structField struct {
	Var *types.Var
	Tag string
}

func makeFuncBody(in types.Object, out types.Object, opts StructOpts) func(group *j.Group) {
	return func(bodyGroup *j.Group) {
		if opts.InTypePointer {
			bodyGroup.If(j.Id(opts.InArgumentName).Op("==").Nil()).Block(
				j.Comment("Decide what to do if input argument is nil"),
			)
			bodyGroup.Line()
		}

		lhsStruct := out.Type().(*types.Named).Underlying().(*types.Struct)
		rhsStruct := in.Type().(*types.Named).Underlying().(*types.Struct)
		lhsFields := extractFields(lhsStruct)
		rhsFields := extractFields(rhsStruct)

		fieldMappings := make(j.Dict)

		fillMappingsDict(fieldMappings, lhsFields, rhsFields, opts.InArgumentName, 0)

		bodyGroup.ReturnFunc(func(returnGroup *j.Group) {
			structureInstantiation := j.Empty()
			if opts.OutTypePointer {
				structureInstantiation.Op("&")
			}

			structureInstantiation.Add(makeQualifier(out.Type())).Values(fieldMappings)
			returnGroup.Add(structureInstantiation)
			if opts.ErrorInReturn {
				returnGroup.Nil()
			}
		})
	}
}

// fillMappingsDict traverses left hand side fields and calls matchFields for each one of them.
func fillMappingsDict(
	mappingDict j.Dict,
	lhsFields, rhsFields []structField,
	rhsSelector string,
	depth int,
) {
	for _, lhsField := range lhsFields {
		// If it's the embedded structure on the 0th level
		if lhsField.Var.Embedded() && depth == 0 {
			embeddedStructure, ok := isStructure(lhsField.Var.Type())
			if ok {
				lhsEmbeddedFields := extractFields(embeddedStructure)
				embeddedDict := make(j.Dict)

				mappingDict[j.Id(lhsField.Var.Id())] = makeQualifier(lhsField.Var.Type()).Values(embeddedDict)

				fillMappingsDict(embeddedDict, lhsEmbeddedFields, rhsFields, rhsSelector, depth+1)
				continue
			}
		}

		matched := matchFields(mappingDict, lhsField, rhsFields, rhsSelector, 0)
		if !matched {

			mappingDict[j.Id(lhsField.Var.Id())] = getDefaultStatementValue(lhsField.Var.Type()).
				Op(",").Comment("TODO: Couldn't map the field; fix it manually")
		}
	}
}

func matchFields(
	mappingDict j.Dict,
	lhsField structField,
	rhsFields []structField,
	rhsSelector string,
	depth int,
) bool {
	lhsId := lhsField.Var.Id()
	for _, rhsField := range rhsFields {

		// If it's the embedded structure on the 0th level
		// try to match embedded values
		if rhsField.Var.Embedded() && depth == 0 {
			embeddedStructure, ok := isStructure(rhsField.Var.Type())
			if ok {
				rhsEmbeddedFields := extractFields(embeddedStructure)
				// embedded fields of the 0th level can be accessed with the same selector
				matched := matchFields(mappingDict, lhsField, rhsEmbeddedFields, rhsSelector, depth+1)
				if matched {
					return true
				}
				continue
			}
		}

		rhsId := rhsField.Var.Id()
		isMatch := matchFieldNames(lhsId, lhsField.Tag, rhsId, rhsField.Tag)
		if !isMatch {
			continue
		}

		assignable := types.AssignableTo(rhsField.Var.Type(), lhsField.Var.Type())
		convertible := types.ConvertibleTo(rhsField.Var.Type(), lhsField.Var.Type())
		if !(assignable || convertible) || !rhsField.Var.Exported() {
			continue
		}

		mappingDict[j.Id(lhsId)] = j.Id(rhsSelector).Dot(rhsId)
		return true
	}

	return false
}

func getDefaultStatementValue(t types.Type) *j.Statement {
	switch t.(type) {
	case *types.Pointer, *types.Slice, *types.Array, *types.Chan, *types.Signature, *types.Interface, *types.Map:
		return j.Nil()

	case *types.Basic:
		switch t.(*types.Basic).Kind() {
		case types.Bool, types.UntypedBool:
			return j.False()
		case types.UnsafePointer:
			return j.Nil()
		case types.String:
			return j.Lit("")
		case types.Int, types.Int8, types.Int16, types.Int32, types.Int64, types.Uint, types.Uint8, types.Uint16, types.Uint32, types.Uintptr,
			types.Uint64, types.Float32, types.Float64, types.Complex64, types.Complex128:
			return j.Lit(0)
		}

	case *types.Struct, *types.Named:
		return makeQualifier(t).Values()
	}

	return j.Comment("couldn't determine")
}

func isStructure(t types.Type) (*types.Struct, bool) {
	named, ok := t.(*types.Named)
	if !ok {
		return nil, false
	}

	structure, ok := named.Underlying().(*types.Struct)
	if !ok {
		return nil, false
	}

	return structure, true
}

func makeQualifier(t types.Type) *j.Statement {
	stringType := types.TypeString(t, nil)

	lastDot := strings.LastIndex(stringType, ".")
	if lastDot == -1 {
		return j.Id(stringType)
	}

	return j.Qual(stringType[:lastDot], stringType[lastDot+1:])
}

func extractFields(structure *types.Struct) []structField {
	var structureFields []structField

	for i := 0; i < structure.NumFields(); i++ {
		field := structure.Field(i)
		tag := structure.Tag(i)

		structureFields = append(structureFields, structField{
			Var: field,
			Tag: extractTagValue(tag),
		})
	}

	return structureFields
}

// matchFieldNames checks if left hand side structFields name is the same as right hand side's name or tag.
func matchFieldNames(lhsId, lhsTag, rhsId, rhsTag string) bool {
	lhsId = strings.ToLower(lhsId)
	lhsTag = strings.ToLower(lhsTag)
	rhsId = strings.ToLower(rhsId)
	rhsTag = strings.ToLower(rhsTag)

	if lhsId == rhsId || lhsId == rhsTag || rhsId == lhsTag {
		return true
	}

	return false
}

// extractTag extracts value of tagKey from a list of tags.
// If there's no tag, returns an empty string.
func extractTagValue(tagString string) string {
	// list of tags looks like this: tag:"x,y" asn1:"explicit"
	tags := strings.Split(tagString, " ")
	for _, tag := range tags {
		tagParts := strings.Split(tag, ":")
		// if tag is invalid
		if len(tagParts) != 2 {
			continue
		}

		tagName := tagParts[0]
		tagValue := tagParts[1]

		if tagName == tagKey {
			return strings.Trim(tagValue, `"`)
		}
	}

	return ""
}

// validateStructs checks that given objects are indeed are structures.
func validateStructs(in, out types.Object) error {
	if in == nil {
		return fmt.Errorf("in object is nil")
	}

	if out == nil {
		return fmt.Errorf("out object is nil")
	}

	lhsNamed, ok := out.Type().(*types.Named)
	if !ok {
		return fmt.Errorf("'out' isn't a structure")
	}

	rhsNamed, ok := in.Type().(*types.Named)
	if !ok {
		return fmt.Errorf("'in' isn't a structure")
	}

	lhsStruct, ok := lhsNamed.Underlying().(*types.Struct)
	if !ok {
		return fmt.Errorf("'out' isn't a structure")
	}

	rhsStruct, ok := rhsNamed.Underlying().(*types.Struct)
	if !ok {
		return fmt.Errorf("'in' isn't a structure")
	}

	if lhsStruct.NumFields() == 0 {
		return fmt.Errorf("'out' structure has no fields")
	}

	if rhsStruct.NumFields() == 0 {
		return fmt.Errorf("'in' structure has no fields")
	}

	if !in.Exported() {
		return fmt.Errorf("'in' structure can't be imported because it's private")
	}

	if !out.Exported() {
		return fmt.Errorf("'out' structure can't be imported because it's private")
	}

	return nil
}

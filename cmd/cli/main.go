package main

import (
	"fmt"
	"log"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
	generatemapping "gitlab.com/vaardan/mappify/internal/generate-mapping"
	"gitlab.com/vaardan/mappify/internal/parse"
)

const (
	defaultFuncName  = "mapStruct"
	defaultInArgName = "in"
)

func main() {
	structCmd := structCommand()
	replCmd := replCommand()

	rootCmd := &cobra.Command{}
	rootCmd.AddCommand(structCmd, replCmd)

	err := rootCmd.Execute()
	if err != nil {
		log.Fatal("execute:", err)
	}
}

func structCommand() *cobra.Command {
	var (
		funcName       string
		inArgName      string
		inTypePointer  bool
		outTypePointer bool
		errorInReturn  bool
	)

	command := &cobra.Command{
		Use:   "struct in_struct_path in_struct_name out_struct_path out_struct_name",
		Short: "Generate structure mapper",
		Long: "Generates function that converts 'in' structure to 'out' structure. Paths to structures can be absolute," +
			"relative, or canonical(i.e. github.com/vaardan/mappify/domain/subfolber).",
		Args: cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			inPath, inName := args[0], args[1]
			outPath, outName := args[2], args[3]

			in, err := parse.Struct(inName, inPath)
			if err != nil {
				log.Fatal(err)
			}

			out, err := parse.Struct(outName, outPath)
			if err != nil {
				log.Fatal(err)
			}

			generatedCode, err := generatemapping.Struct(in, out, generatemapping.StructOpts{
				FunctionName:   funcName,
				InArgumentName: inArgName,
				InTypePointer:  inTypePointer,
				OutTypePointer: outTypePointer,
				ErrorInReturn:  errorInReturn,
			})
			if err != nil {
				log.Fatal(err)
			}

			fmt.Print(generatedCode)
		},
	}

	flags := command.Flags()
	flags.StringVarP(&funcName, "func-name", "n", defaultFuncName, "names mapper function")
	flags.StringVar(&inArgName, "in-arg-name", defaultInArgName, "names function 'in' argument")
	flags.BoolVar(&inTypePointer, "in-pointer", false, "makes 'in' structure's type a pointer")
	flags.BoolVar(&outTypePointer, "out-pointer", false, "makes 'out' structure's type a pointer")
	flags.BoolVarP(&errorInReturn, "error-arg", "e", false, "function will have error in return types")

	return command
}

func replCommand() *cobra.Command {
	answers := struct {
		InStructPath   string `survey:"in_struct_path"`
		InStructName   string `survey:"in_struct_name"`
		OutStructPath  string `survey:"out_struct_path"`
		OutStructName  string `survey:"out_struct_name"`
		FuncName       string `survey:"func_name"`
		InArgName      string `survey:"in_arg_name"`
		InTypePointer  bool   `survey:"in_type_pointer"`
		OutTypePointer bool   `survey:"out_type_pointer"`
		ErrorInReturn  bool   `survey:"error_in_return"`
	}{}

	questions := []*survey.Question{
		{
			Name:     "in_struct_path",
			Prompt:   &survey.Input{Message: "Path to the 'in' structure"},
			Validate: survey.Required,
		},
		{
			Name:     "in_struct_name",
			Prompt:   &survey.Input{Message: "Name of the 'in' structure"},
			Validate: survey.Required,
		},
		{
			Name:     "out_struct_path",
			Prompt:   &survey.Input{Message: "Path to the 'out' structure"},
			Validate: survey.Required,
		},
		{
			Name:     "out_struct_name",
			Prompt:   &survey.Input{Message: "Name of the 'out' structure"},
			Validate: survey.Required,
		},
		{
			Name: "func_name",
			Prompt: &survey.Input{
				Message: "Name the mapper function",
				Default: defaultFuncName,
			},
		},
		{
			Name: "in_arg_name",
			Prompt: &survey.Input{
				Message: "Name function's input argument",
				Default: defaultInArgName,
			},
		},
		{
			Name: "in_type_pointer",
			Prompt: &survey.Confirm{
				Message: "Is 'in' structure type a pointer?",
				Default: false,
			},
		},
		{
			Name: "out_type_pointer",
			Prompt: &survey.Confirm{
				Message: "Is 'out' structure type a pointer?",
				Default: false,
			},
		},
		{
			Name: "error_in_return",
			Prompt: &survey.Confirm{
				Message: "Does function returns error as a second argument?",
				Default: false,
			},
		},
	}

	command := &cobra.Command{
		Use:   "repl",
		Short: "Enter REPL mode",
		Long:  "Enter REPL mode and enter parameters one by one",
		Run: func(cmd *cobra.Command, args []string) {
			err := survey.Ask(questions, &answers)
			if err != nil {
				log.Fatal(err)
			}

			in, err := parse.Struct(answers.InStructName, answers.InStructPath)
			if err != nil {
				log.Fatal(err)
			}

			out, err := parse.Struct(answers.OutStructName, answers.OutStructPath)
			if err != nil {
				log.Fatal(err)
			}

			generatedCode, err := generatemapping.Struct(in, out, generatemapping.StructOpts{
				FunctionName:   answers.FuncName,
				InArgumentName: answers.InArgName,
				InTypePointer:  answers.InTypePointer,
				OutTypePointer: answers.OutTypePointer,
				ErrorInReturn:  answers.ErrorInReturn,
			})
			if err != nil {
				log.Fatal(err)
			}

			fmt.Print(generatedCode)
		},
	}

	return command
}
